import React from 'react';
import { View } from 'react-native';

const StatusBar = () => {
    const { statusbar } = styles;

    return (
        <View style={statusbar}/>
    )
};

const styles = {
    statusbar: {
        backgroundColor: '#fff',
        height: 22,
    },
};

export { StatusBar };