import React, { Component } from 'react';
import {
    Text,
    View,
} from 'react-native';

const Header = ({header}) => {
    const {headerBar, headerText} = styles;

    return (
        <View style={headerBar}>
            <Text style={headerText}>
                {header}
            </Text>
        </View>
    )

};

const styles = {
    headerText: {
        color: '#444',
        fontSize: 16,
        fontWeight: "500"
    },
    headerBar: {
        alignItems: 'center',
        backgroundColor: '#fff',
        borderBottomColor: '#eee',
        borderColor: 'transparent',
        borderWidth: 1,
        justifyContent: 'center',
        height: 44,
        flexDirection: 'row'
    },
};

export { Header };
