/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    AsyncStorage,
    TextInput,
    TouchableOpacity
} from 'react-native';
import FloatingActionButton from 'react-native-action-button';
import { SwipeListView } from 'react-native-swipe-list-view';
import { Header, StatusBar} from "./components";
import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyAgurOq3XqQ_GW4u_vdHJvika22TQvEDP0",
    authDomain: "sgopp-fb269.firebaseapp.com",
    databaseURL: "https://sgopp-fb269.firebaseio.com",
    projectId: "sgopp-fb269",
    storageBucket: "",
    messagingSenderId: "285097580688"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);


let Items = {
    convertToArrayOfObject(items, callback) {
        return callback(
            items ? items.split("||").map((item, i) => ({ key: i, text: item })) : []
        );
    },
    convertToStringWithSeparators(items) {
        return items.map(item => item.text).join("||");
    },
    all(callback) {
        return AsyncStorage.getItem("ITEMS", (err, items) =>
            this.convertToArrayOfObject(items, callback)
        );
    },
    save(items) {
        AsyncStorage.setItem("ITEMS", this.convertToStringWithSeparators(items));
    }
};

type Props = {};
export default class App extends Component<Props> {
    state = {
        items: [],
        text: ""
    };

    componentDidMount() {
        Items.all(items => this.setState({ items: items || [] }));
    }

    changeTextHandler = text => {
        this.setState({ text: text });
    };

    addItem = () => {
        let notEmpty = this.state.text.trim().length > 0;

        if (notEmpty) {
            this.setState(
                prevState => {
                    let { items, text } = prevState;
                    return {
                        items: items.concat({ key: items.length, text: text }),
                        text: ""
                    };
                }, () => Items.save(this.state.items)
            );
        }
    };

    deleteItem = i => {
        this.setState(
            prevState => {
                let items = prevState.items.slice();

                items.splice(i, 1);

                return { items: items };
            }, () => Items.save(this.state.items)
        );
    };

    getRef() {
        return firebaseApp.database().ref();
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar/>
                <Header header="Shopping List"/>
                <SwipeListView
                    useFlatList
                    data={this.state.items}
                    renderItem={ ({ item }) => (
                        <TouchableHighlight style={styles.listItem}>
                            <Text style={styles.listItemText}>
                                {item.text}
                            </Text>
                        </TouchableHighlight>
                    )}
                    renderHiddenItem={ ({ index }) => (
                        <View style={styles.rowBack}>
                            <TouchableOpacity style={[styles.backRightBtn, styles.backRightBtnRight]} onPress={() => this.deleteItem(index)}>
                                <Text style={styles.backTextWhite}>Delete</Text>
                            </TouchableOpacity>
                        </View>
                    )}
                    disableRightSwipe={true}
                    rightOpenValue={-75}
                    previewRowKey={'0'}
                />
                <TextInput
                    style={styles.textInput}
                    onChangeText={this.changeTextHandler}
                    onSubmitEditing={this.addItem}
                    value={this.state.text}
                    placeholder="Add Items"
                    returnKeyType="done"
                    returnKeyLabel="done"
                />



                {/*<FloatingActionButton
                    hideShadow={true}
                    buttonColor="rgba(2,31,249,1)"
                    onPress={}
                />*/}





            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },

    headerText: {
        color: '#444',
        fontSize: 16,
        fontWeight: "500"
    },
    headerBar: {
        alignItems: 'center',
        backgroundColor: '#fff',
        borderBottomColor: '#eee',
        borderColor: 'transparent',
        borderWidth: 1,
        justifyContent: 'center',
        height: 44,
        flexDirection: 'row'
    },
    listItem: {
        backgroundColor: '#fff',
        borderBottomColor: '#eee',
        borderColor: 'transparent',
        borderWidth: 1,
        paddingLeft: 16,
        paddingTop: 14,
        paddingBottom: 16,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    listItemText: {
        color: '#333',
        fontSize: 16,
    },
    textInput: {
        height: 40,
        paddingRight: 10,
        paddingLeft: 10,
        borderColor: "gray",
        width: "100%"
    },

    rowBack: {
        alignItems: 'center',
        backgroundColor: 'red',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 15,
    },

    backRightBtnRight: {
        backgroundColor: 'red',
        right: 0
    },

    backRightBtn: {
        alignItems: 'center',
        bottom: 0,
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        width: 75
    },

    backTextWhite: {
        color: '#FFF'
    },


});
